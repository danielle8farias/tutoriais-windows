# Índice

0. [O Windows não pode ser instalado em partições GPT](p0000_format_particao_gpt.md)

1. [Instalando, configurando e inicializando o Git no Windows](p0001_instalacao_git_windows.md)

2. [Instalação do GitHub Desktop para Windows](p0002_github_desktop.md)

3. [Instalando o VS Code no Windows](p0003_vscode_windows.md)

