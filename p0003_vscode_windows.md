# [Tutorial] Instalando o VS Code no Windows


No seu navegador de preferência digite o site oficial do [VSCode](https://code.visualstudio.com/).

Em seguida procure pelo botão de **Download**.

![página de download do vscode](img/p0003-0.png)

Na tela seguinte, escolha a arquitetura do seu Windows. No meu caso é a de 64 bits.

![escolhendo o arquivo segundo a arquitetura](img/p0003-1.png)

Você será redirecionada para uma nova tela

![página de download](img/p0003-2.png)

Se o download não começar logo em seguida, **clique no link em destaque na imagem**.

Vá até a sua pasta de downloads e procure pelo **instalador do VSCode** que você acabou de baixar e dê **dois cliques** para ativá-lo.

![instalador do VSCode](img/p0003-3.png)

Leia o acordo de licença, **clique na aceitação e no botão próximo**.

![acordo de licença](img/p0003-4.png)

Selecione o **destino da instalação** (caso queira uma diferente daquela sugerida pelo instalador) e clique em próximo.

![destino da instalação](img/p0003-5.png)

Em seguida será perguntado sobre a **criação de atalhos para o lançador** do programa. Eu decidi manter o padrão. Clique em próximo.

![criação dos atalhos](img/p0003-6.png)

Será apresentado uma tela para marcar algumas **opções de tarefas adicionais** para o VSCode. Eu marquei conforme a imagem, mas você pode configurar como achar melhor para si.

![tela de tarefas adicionais](img/p0003-7.png)

Em seguida, será mostrado as **configurações iniciais** feitas pela usuária, caso deseje fazer alguma revisão.

![configurações iniciais](img/p0003-8.png)

A instalação irá começar

![início da instalação](img/p0003-9.png)

Ao concluir, você pode deixar marcada a parte de iniciar o VSCode logo em seguida ou desmarcá-la.

![conclusão da instalação](img/p0003-10.png)

Sua instalação foi concluída com sucesso! =)
